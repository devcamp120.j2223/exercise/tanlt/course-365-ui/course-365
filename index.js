//Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

var mongoose = require('mongoose');

const courseModel = require("./app/models/courseModel");

//Import router
const courseRouter = require("./app/routes/courseRouter.js");

//Khởi tạo app express
const app = express();

const port = 8000;

//Khai báo middleware đọc json
app.use(express.json());

//Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

mongoose.connect("mongodb://localhost:27017/CRUD_Course365", (error) => {
    if (error) { 
        throw error 
    };
    console.log('Connect MongooDB successfully!');
})

app.use(express.static(__dirname + '/views'));

//Khai báo API dạng get "/" sẽ chạy vào đây
//Callback funtion
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/index.html'));
})

app.use("/", courseRouter);

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng): " + port)
})