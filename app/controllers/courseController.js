// Import course model vao controller
const courseModel = require("../models/courseModel");

// Import mongooseJS
const mongoose = require("mongoose");

const createCourse = (request, response) => {
    //B1: Thu thập dữ liệu
    var bodyRequest = request.body;
    console.log(bodyRequest);
    //B2: Kiểm tra dữ liệu
    if (!bodyRequest.courseCode) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "courseCode is required"
        })
    }
    if (!bodyRequest.courseName) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "courseName is required"
        })
    }
    if (!(bodyRequest.price && bodyRequest.price > 0)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "price is not valid"
        })
    }
    if (!(bodyRequest.discountPrice && bodyRequest.discountPrice > 0)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "discountPrice is not valid"
        })
    }
    if (!bodyRequest.duration) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "duration is required"
        })
    }
    if (!bodyRequest.level) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "level is required"
        })
    }
    if (!bodyRequest.coverImage) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "coverImage is required"
        })
    }
    if (!bodyRequest.teacherName) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "teacherName is required"
        })
    }
    if (!bodyRequest.teacherPhoto) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "teacherPhoto is required"
        })
    }

    //Thao tác với cơ sở dữ liệu
    let createCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: bodyRequest.courseCode,
        courseName: bodyRequest.courseName,
        price: bodyRequest.price,
        discountPrice: bodyRequest.discountPrice,
        duration: bodyRequest.duration,
        level: bodyRequest.level,
        coverImage: bodyRequest.coverImage,
        teacherName: bodyRequest.teacherName,
        teacherPhoto: bodyRequest.teacherPhoto,
    }

    courseModel.create(createCourse, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(201).json({
                status: "Success: Course Created",
                data: data
            })
        }
    })

}

const getAllCourse = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
    //B3 Thao tác với cơ sỡ dữ liệu
    courseModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(202).json({
                description: "This DB includes all courses in system",
                courses: data
            })
        }
    })
}

const getCourseById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let courseId = request.params.courseId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(courseId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    courseModel.findById(courseId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                status: "Success: Get courses sucess",
                data: data
            })
        }
    })
}

const updateCourseById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let courseId = request.params.courseId;
    var bodyRequest = request.body;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(courseId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    let courseUpdate = {
        courseCode: bodyRequest.courseCode,
        courseName: bodyRequest.courseName,
        price: bodyRequest.price,
        discountPrice: bodyRequest.discountPrice,
        duration: bodyRequest.duration,
        level: bodyRequest.level,
        coverImage: bodyRequest.coverImage,
        teacherName: bodyRequest.teacherName,
        teacherPhoto: bodyRequest.teacherPhoto,
    }
    courseModel.findByIdAndUpdate(courseId, courseUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(201).json({
                status: "Success: Update course success",
                data: data
            })
        }
    })
}

const deleteCourseById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let courseId = request.params.courseId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(courseId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(204).json({
                status: "Success: Delete courses sucess",
            })
        }
    })
}

module.exports = {
    createCourse: createCourse,
    getAllCourse: getAllCourse,
    getCourseById: getCourseById,
    updateCourseById: updateCourseById,
    deleteCourseById: deleteCourseById
}