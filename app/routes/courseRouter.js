
const express = require("express");

const router = express.Router();
const { createCourse, getAllCourse, getCourseById, updateCourseById, deleteCourseById } = require("../controllers/courseController")

router.get("/courses", getAllCourse)

router.post("/courses", createCourse);

router.get("/courses/:courseId", getCourseById)

router.put("/courses/:courseId", updateCourseById)

router.delete("/courses/:courseId", deleteCourseById)

//Export dữ liệu thành 1 module
module.exports = router;