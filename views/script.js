var gCoursesDB = {
}
function onPageLoading() {
    callApi();
    loadPopular();
    loadTrending();
}
function callApi(){
    $.ajax({
        url: "http://localhost:8000/courses",
        type: 'GET',
        dataType: 'json', // added data type
        async: false,
        success: function (responseObject) {
            gCoursesDB = responseObject;
            console.log(gCoursesDB);
        },
        error: function (ajaxContext) {
          alert(ajaxContext)
        }
      });
}

function loadPopular() {
    var vPopular = [];
    for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
        if (gCoursesDB.courses[bI].isPopular) {
            vPopular.push(gCoursesDB.courses[bI]);
        }
    }
    for (var bIndex = 0; bIndex < vPopular.length; bIndex++) {
        //Ảnh nền môn học
        $("#img-pop-" + (bIndex + 1)).attr("src", vPopular[bIndex].coverImage);

        //Tên môn học
        $("#a-name-pop-" + (bIndex + 1)).html(vPopular[bIndex].courseName);

        //Thời gian
        $("#p-time-pop-" + (bIndex + 1)).html('<i class="fas fa-clock"></i>&nbsp;' + vPopular[bIndex].duration + " " + vPopular[bIndex].level);

        //Học phí
        $("#span-price-" + (bIndex + 1)).html("$" + vPopular[bIndex].discountPrice + "&nbsp;&nbsp;&nbsp;");

        $("#span-price-sale-" + (bIndex + 1)).html("$" + vPopular[bIndex].price);

        //Ảnh giáo viên
        $("#img-teacher-pop-" + (bIndex + 1)).attr("src", vPopular[bIndex].teacherPhoto);

        //Tên giáo viên
        $("#span-pop-" + (bIndex + 1)).html(vPopular[bIndex].teacherName);
    }
}

function loadTrending() {
    var vTrending = [];
    for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
        if (gCoursesDB.courses[bI].isTrending) {
            vTrending.push(gCoursesDB.courses[bI]);
        }
    }
    for (var bIndex = 0; bIndex < vTrending.length; bIndex++) {
        //Ảnh nền môn học
        $("#img-trend-" + (bIndex + 1)).attr("src", vTrending[bIndex].coverImage);
    
        //Tên môn học
        $("#a-name-trend-" + (bIndex + 1)).html(vTrending[bIndex].courseName);
    
        //Thời gian
        $("#p-time-trend-" + (bIndex + 1)).html('<i class="fas fa-clock"></i>&nbsp;' + vTrending[bIndex].duration + " " + vTrending[bIndex].level);
    
        //Học phí
        $("#span-trendprice-" + (bIndex + 1)).html("$" + vTrending[bIndex].discountPrice + "&nbsp;&nbsp;&nbsp;");
    
        $("#span-trendprice-sale-" + (bIndex + 1)).html("$" + vTrending[bIndex].price);
    
        //Ảnh giáo viên
        $("#img-teacher-trend-" + (bIndex + 1)).attr("src", vTrending[bIndex].teacherPhoto);
    
        //Tên giáo viên
        $("#span-trend-" + (bIndex + 1)).html(vTrending[bIndex].teacherName);
    }
}

function loadCourses(paramList) {
    for (var bI = 0; bI < paramList.length; bI++) {
        $('.card-new').append(
            `<div class="col-sm-3 mt-4">
            <div class="card">
            <img id="img-trend-`+ paramList[bI]._id + `" class="card-img-top" src=` + paramList[bI].coverImage + ` alt="Card image cap">
            <div class="card-body">
              <a id="a-name-trend-`+ paramList[bI]._id + `" class="font-weight-bold" href="#">` + paramList[bI].courseName + `</a>
              <p id="p-time-trend-`+ paramList[bI]._id + `" class="card-text mt-2"><i class="fas fa-clock"></i>&nbsp;` + paramList[bI].duration + " " + paramList[bI].level + `</p>
              <span id="span-trendprice-`+ paramList[bI]._id + `" class="font-weight-bold">$` + paramList[bI].discountPrice + `&nbsp;</span><span id="span-trendprice-sale-` + paramList[bI]._id + `" class="gachngang">$` + paramList[bI].price + `</span>
            </div>
            <div class="card-footer text-muted">
              <div class="row">
                <div class="col-sm-10">
                  <img id="img-teacher-trend-`+ paramList[bI]._id + `" src=` + paramList[bI].teacherPhoto + `
                    class="img-avt rounded-circle">&nbsp;&nbsp;&nbsp;<span id="span-trend-`+ bI + `">` + paramList[bI].teacherName + `</span>
                </div>
                <div class="col-sm-1">
                  <i class="far fa-bookmark"></i>
                </div>
              </div>
              <div class="row mt-2 d-flex justify-content-end">
                <button id="btn-edit`+ paramList[bI]._id + `" class="btn-edit btn btn-primary">Edit</button>
                <button id="btn-delete`+ paramList[bI]._id + `" class="btn-delete btn btn-danger">Delete</button>
              </div>
              </div>
            </div>
          </div>
        </div>`);
    }
}