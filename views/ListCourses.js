var gCoursesDB = {
}
var gId = -1;
var gIdDelete = -1
$(document).ready(function () {

    //Thêm C
    $("#btn-add-modal").on("click", function () {
        $("#create-modal").modal("show");
    })
    $("#btn-add").on("click", function () {
        onBtnInsertClick();
    });

    //Đọc R
    onPageLoading();

    //Sửa U
    $(document).on("click", ".btn-edit", function () {
        console.log("Nút sửa được bấm ");
        gId = onBtnClick(this);
        showInfoUpdate(gId);
        $("#update-modal").modal("show");
    })
    $("#btn-update").on("click", function () {
        onBtnUpdateClick();
    });

    //Xóa D
    $(document).on("click", ".btn-delete", function () {
        console.log("Nút xóa được bấm ");
        gIdDelete = onBtnDelClick(this);
        console.log(gIdDelete);
        $("#delete-confirm-modal").modal("show");
    })
    $("#btn-confirm-delete").on("click", function () {
        onBtnDeleteClick();
    });

    function onBtnInsertClick() {
        var vDataObj = {
        }
        console.log(gId);
        getInfo(vDataObj);
        if (validateObj(vDataObj)) {
            $('.card-new').empty();
            callApiInsert(vDataObj);
            onPageLoading();
            $("#create-modal").modal("hide");
            resetModal();
        };
    }
    function onBtnUpdateClick() {
        var vObj = {}
        dataUpdate(vObj);
        if (validateObjUpdate(vObj)) {
            $('.card-new').empty();
            callApiUpdate(gId, vObj)
            onPageLoading();
            $("#update-modal").modal("hide");
            resetModal();
        };
    }
    function onBtnDeleteClick() {
        callApiDelete(gIdDelete);
        // B4: xử lý front-end
        $('.card-new').empty();
        onPageLoading();
        $("#delete-confirm-modal").modal("hide");
    }
    function onPageLoading() {
        callApi();
        loadCourses();
    }
    function loadCourses() {
        for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
            $('.card-new').append(
                `<div class="col-sm-3 mt-4">
                <div class="card">
                <img id="img-trend-`+ gCoursesDB.courses[bI]._id + `" class="card-img-top" src=` + gCoursesDB.courses[bI].coverImage + ` alt="Card image cap">
                <div class="card-body">
                  <a id="a-name-trend-`+ gCoursesDB.courses[bI]._id + `" class="font-weight-bold" href="#">` + gCoursesDB.courses[bI].courseName + `</a>
                  <p id="p-time-trend-`+ gCoursesDB.courses[bI]._id + `" class="card-text mt-2"><i class="fas fa-clock"></i>&nbsp;` + gCoursesDB.courses[bI].duration + " " + gCoursesDB.courses[bI].level + `</p>
                  <span id="span-trendprice-`+ gCoursesDB.courses[bI]._id + `" class="font-weight-bold">$` + gCoursesDB.courses[bI].discountPrice + `&nbsp;</span><span id="span-trendprice-sale-` + gCoursesDB.courses[bI]._id + `" class="gachngang">$` + gCoursesDB.courses[bI].price + `</span>
                </div>
                <div class="card-footer text-muted">
                  <div class="row">
                    <div class="col-sm-10">
                      <img id="img-teacher-trend-`+ gCoursesDB.courses[bI]._id + `" src=` + gCoursesDB.courses[bI].teacherPhoto + `
                        class="img-avt rounded-circle">&nbsp;&nbsp;&nbsp;<span id="span-trend-`+ bI + `">` + gCoursesDB.courses[bI].teacherName + `</span>
                    </div>
                    <div class="col-sm-1">
                      <i class="far fa-bookmark"></i>
                    </div>
                  </div>
                  <div class="row mt-2 d-flex justify-content-end">
                    <button id="btn-edit`+ gCoursesDB.courses[bI]._id + `" class="btn-edit btn btn-primary">Edit</button>
                    <button id="btn-delete`+ gCoursesDB.courses[bI]._id + `" class="btn-delete btn btn-danger">Delete</button>
                  </div>
                  </div>
                </div>
              </div>
            </div>`);
        }
    }

    // hàm sinh ra đc id tự tăng tiếp theo, dùng khi Thêm mới phần tử
    function getInfo(paramObj) {
        paramObj.courseCode = $("#inp-courseCode").val().trim();
        paramObj.courseName = $("#inp-courseName").val().trim();

        paramObj.price = $("#inp-price").val().trim();
        paramObj.discountPrice = $("#inp-discountPrice").val().trim();
        paramObj.duration = $("#inp-duration").val().trim();

        paramObj.level = $("#inp-level").val().trim();
        paramObj.coverImage = $("#inp-coverImage").val().trim();
        paramObj.teacherName = $("#inp-teacherName").val().trim();

        paramObj.teacherPhoto = $("#inp-teacherPhoto").val().trim();
        paramObj.isPopular = $("#select-isPopular").val();
        paramObj.isTrending = $("#select-isTrending").val();
    }
    function validateObj(paramObj) {
        if (paramObj.courseCode == "") {
            console.log("Chưa nhập courseCode");
            return false;
        }
        else if (paramObj.courseName == "") {
            console.log("Chưa nhập courseName");
            return false;
        }
        else if (paramObj.price == "") {
            console.log("Chưa nhập price");
            return false;
        }
        else if (paramObj.discountPrice == "") {
            console.log("Chưa nhập discountPrice");
            return false;
        }
        else if (paramObj.duration == "") {
            console.log("Chưa nhập duration");
            return false;
        }
        else if (paramObj.level == "") {
            console.log("Chưa nhập level");
            return false;
        }
        else if (paramObj.coverImage == "") {
            console.log("Chưa nhập coverImage");
            return false;
        }
        else if (paramObj.teacherName == "") {
            console.log("Chưa nhập teacherName");
            return false;
        }
        else if (paramObj.teacherPhoto == "") {
            console.log("Chưa nhập teacherPhoto");
            return false;
        }
        return true;
    }
    function resetModal() {
        $("#inp-courseCode").val("");
        $("#inp-courseName").val("");

        $("#inp-price").val("");
        $("#inp-discountPrice").val("");
        $("#inp-duration").val("");

        $("#inp-level").val("");
        $("#inp-coverImage").val("");
        $("#inp-teacherName").val("");

        $("#inp-teacherPhoto").val("");
        $("#select-isPopular").val("");
        $("#select-isTrending").val("");
    }

    function onBtnClick(paramElement) {
        "use strict";
        var vClick = $(paramElement).attr('id');
        var vId = vClick.split("btn-edit");
        return vId[1];
    }

    function onBtnDelClick(paramElement) {
        "use strict";
        var vClick = $(paramElement).attr('id');
        var vId = vClick.split("btn-delete");
        return vId[1];
    }
    function showInfoUpdate(paramId) {
        var vCourse = callApiGetById(paramId)
        $("#inp-courseCode-update").val(vCourse.courseCode);
        $("#inp-courseName-update").val(vCourse.courseName);

        $("#inp-price-update").val(vCourse.price);
        $("#inp-discountPrice-update").val(vCourse.discountPrice);
        $("#inp-duration-update").val(vCourse.duration);

        $("#inp-level-update").val(vCourse.level);
        $("#inp-coverImage-update").val(vCourse.coverImage);
        $("#inp-teacherName-update").val(vCourse.teacherName);

        $("#inp-teacherPhoto-update").val(vCourse.teacherPhoto);

        if (vCourse.isPopular) {
            $("#select-isPopular-update").val(1);
        }
        else { $("#select-isPopular-update").val(0); }

        if (vCourse.isTrending) {
            $("#select-isTrending-update").val(1);
        }
        else { $("#select-isTrending-update").val(0); }

    }
    function validateObjUpdate(paramObj) {
        if (paramObj.courseCode == "") {
            console.log("Chưa nhập courseCode");
            return false;
        }
        else if (paramObj.courseName == "") {
            console.log("Chưa nhập courseName");
            return false;
        }
        else if (paramObj.price == "") {
            console.log("Chưa nhập price");
            return false;
        }
        else if (paramObj.discountPrice == "") {
            console.log("Chưa nhập discountPrice");
            return false;
        }
        else if (paramObj.duration == "") {
            console.log("Chưa nhập duration");
            return false;
        }
        else if (paramObj.level == "") {
            console.log("Chưa nhập level");
            return false;
        }
        else if (paramObj.coverImage == "") {
            console.log("Chưa nhập coverImage");
            return false;
        }
        else if (paramObj.teacherName == "") {
            console.log("Chưa nhập teacherName");
            return false;
        }
        else if (paramObj.teacherPhoto == "") {
            console.log("Chưa nhập teacherPhoto");
            return false;
        }
        return true;
    }
    function dataUpdate(paramObj) {
        paramObj.courseCode = $("#inp-courseCode-update").val().trim();
        paramObj.courseName = $("#inp-courseName-update").val().trim();

        paramObj.price = $("#inp-price-update").val().trim();
        paramObj.discountPrice = $("#inp-discountPrice-update").val().trim();
        paramObj.duration = $("#inp-duration-update").val().trim();

        paramObj.level = $("#inp-level-update").val().trim();
        paramObj.coverImage = $("#inp-coverImage-update").val().trim();
        paramObj.teacherName = $("#inp-teacherName-update").val().trim();

        paramObj.teacherPhoto = $("#inp-teacherPhoto-update").val().trim();
        if ($("#select-isPopular-update").val()) {
            paramObj.isPopular = true;
        } else {paramObj.isPopular = false}
        if ($("#select-isTrending-update").val()) {
            paramObj.isTrending = true;
        } else {paramObj.isTrending = false;}
    }
    function callApi() {
        $.ajax({
            url: "http://localhost:8000/courses",
            type: 'GET',
            dataType: 'json', // added data type
            async: false,
            success: function (responseObject) {
                gCoursesDB = responseObject;
                console.log(gCoursesDB);
            },
            error: function (ajaxContext) {
                alert(ajaxContext)
            }
        });
    }
    function callApiGetById(paramId) {
        var vResult
        $.ajax({
            url: "http://localhost:8000/courses/" + paramId,
            type: 'GET',
            dataType: 'json', // added data type
            async: false,
            success: function (responseObject) {
                vResult = responseObject.data
                console.log(responseObject);
            },
            error: function (ajaxContext) {
                alert(ajaxContext)
            }
        });
        return vResult
    }

    function callApiInsert(paramObjRequest) {
        $.ajax({
            url: "http://localhost:8000/courses",
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObjRequest),
            async: false,
            success: function (responseObject) {
                console.log(responseObject);
            },
            error: function (ajaxContext) {
                alert("thất bại")
            }
        });
    }

    function callApiUpdate(paramId, paramObjRequest) {
        console.log(paramObjRequest)
        $.ajax({
            url: "http://localhost:8000/courses/" + paramId,
            type: 'PUT',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObjRequest),
            async: false,
            success: function (responseObject) {
                console.log(responseObject);
            },
            error: function (ajaxContext) {
                alert("thất bại")
            }
        });
    }

    function callApiDelete(paramId) {
        $.ajax({
            url: "http://localhost:8000/courses/" + paramId,
            type: 'DELETE',
            contentType: "application/json;charset=UTF-8",
            async: false,
            success: function (responseObject) {
                console.log(responseObject);
            },
            error: function (ajaxContext) {
                alert("thất bại")
            }
        });
    }

})
